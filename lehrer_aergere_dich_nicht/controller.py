import dispatch
import gamesetup
import view


class Controller(object):
    def __init__(self, log):
        """Erstelle ein Controller Objekt."""
        self.__log = log.getChild("ctrl")
        self.__log.debug("initializing Controller")
        self.__game = dispatch.ActorDispatch(
            log,
            self.__vacate_home,
            self.__fill_home,
            self.__vacate_track,
            self.__fill_track,
            self.__vacate_dest,
            self.__fill_dest,
            self.__end_turn,
            self.__begin_turn,
            self.__game_end,
            self.__waiting_done,
            self.__die_animation,
            self.__show_die,
            self.__show_connection_info,
            self.__actor_ready,
            self.__report_error,
            self.__process_view,
        )
        self.__view = view.GUI(
            log,
            self.__set_player_mode,
            self.__setup_done,
            self.__client_set_host,
            self.__client_set_port,
            self.__client_connect,
            self.__roll_die,
            self.__click_move_home,
            self.__click_move_track,
            self.__click_move_dest,
            self.__close_window,
        )
        self.__setup = gamesetup.Setup(
            log, self.__enable_setup_done, self.__enable_client_connect
        )

    def run(self):
        """Starte die Anwendung."""
        self.__log.debug("running Controller")
        self.__view.view_setup()
        self.__view.run()

    def __set_player_mode(self, player: int, mode: int):
        """Setze den Modus für `player`."""
        self.__setup.set_player_mode(player, mode)

    def __enable_setup_done(self, state: bool):
        """De-/aktiviere den Bestätigungsknopf."""
        if state:
            self.__view.enable_start_game()
        else:
            self.__view.disable_start_game()

    def __setup_done(self):
        """Bestätige die Spielkonfiguration."""
        self.__log.debug("setup done")
        self.__view.view_waiting()
        self.__setup.apply(self.__game, True)

    def __client_set_host(self, host: str):
        """Setze den Host."""
        self.__setup.client_set_host(host)

    def __client_set_port(self, port: str):
        """Setze den Port."""
        self.__setup.client_set_port(port)

    def __enable_client_connect(self, state: bool):
        """De-/aktiviere den Bestätigungsknopf."""
        if state:
            self.__view.enable_client_connect()
        else:
            self.__view.disable_client_connect()

    def __client_connect(self):
        """Bestätige die Client-Konfiguration."""
        if self.__setup.apply(self.__game, False):
            self.__view.view_waiting()
            self.__game.run_client()

    def __show_connection_info(self, player: int, port: int):
        """Zeige den Port, an den sich ein Client verbinden soll, an."""
        self.__view.show_connection_info(player, port)

    def __actor_ready(self, player: int):
        """Zeige an, das ein Akteur bereit ist."""
        self.__view.set_player_ready(player)

    def __waiting_done(self):
        """Beginne das Spiel."""
        self.__view.view_board()

    def __roll_die(self):
        """Fordere lokal einen Würfelwurf an."""
        self.__view.disable_die()
        self.__log.debug("die clicked")
        self.__game.click_roll_die()

    def __die_animation(self):
        """Werfe den Würfel."""
        self.__log.debug("die animation")
        self.__view.sleep(5, lambda: self.__die_step(0))

    def __die_step(self, step: int):
        """Führe einen Schritt des Würfelwurfes aus."""
        if step == 8:
            self.__update_die()
            self.__log.debug("allowing move")
            self.__game.allow_move()
        else:
            self.__update_die()
            step += 1
            self.__view.sleep((1 << step), lambda: self.__die_step(step))

    def __update_die(self):
        """Zeige einen neuen Würfelwert an."""
        self.__show_die(self.__game.roll_die())

    def __show_die(self, pips: int):
        """Zeige den Würfelwert an."""
        self.__log.debug("showing die {}".format(pips))
        self.__view.update_die(pips)

    def __click_move_home(self, player: int, field: int):
        """Ein lokaler Nutzer fordert das Bewegen einer Figur an."""
        self.__game.click_move_home(player, field)

    def __click_move_track(self, field: int):
        """Ein lokaler Nutzer fordert das Bewegen einer Figur an."""
        self.__game.click_move_track(field)

    def __click_move_dest(self, player: int, field: int):
        """Ein lokaler Nutzer fordert das Bewegen einer Figur an."""
        self.__game.click_move_dest(player, field)

    def __end_turn(self, player: int):
        """`player`'s Zug ist zu Ende."""
        self.__log.debug("ending player {}'s turn".format(player))
        self.__view.unset_active_player(player)

    def __begin_turn(self, player: int):
        """`player`'s Zug beginnt."""
        self.__log.debug("player {}'s turn".format(player))
        self.__view.set_active_player(player)
        self.__view.enable_die()

    def __vacate_home(self, owner: int, field: int):
        """Leere ein Feld."""
        self.__log.debug("vacating player #{} home #{}".format(owner, field))
        self.__view.vacate_home(owner, field)

    def __fill_home(self, owner: int, field: int, piece: int):
        self.__log.debug("filling player #{} home #{}".format(owner, field))
        self.__view.fill_home(owner, field, piece)

    def __vacate_track(self, field: int):
        """Leere ein Feld."""
        self.__log.debug("vacating track #{}".format(field))
        if field % 10 == 0:
            self.__view.vacate_start(field // 10)
        else:
            self.__view.vacate_track(field)

    def __fill_track(self, field: int, piece: int):
        """Fülle ein Feld."""
        self.__log.debug("filling track #{}".format(field))
        if field % 10 == 0:
            self.__view.fill_start(field // 10, piece)
        else:
            self.__view.fill_track(field, piece)

    def __vacate_dest(self, owner: int, field: int):
        """Leere ein Feld."""
        self.__log.debug("vacating player #{} dest #{}".format(owner, field))
        self.__view.vacate_dest(owner, field)

    def __fill_dest(self, owner: int, field: int, piece: int):
        """Fülle ein Feld."""
        self.__log.debug("filling player #{} dest #{}".format(owner, field))
        self.__view.fill_dest(owner, field, piece)

    def __game_end(self, winner: int):
        """`winner` hat gewonnen."""
        self.__view.announce_winner(winner)

    def __process_view(self):
        """Verarbeite kurzzeitig GUI-Ereignisse."""
        self.__view.process()

    def __report_error(self, message: str):
        """Zeige eine Fehlermeldung an."""
        self.__log.info("error: {}".format(message))
        self.__view.report_error(message)

    def __close_window(self):
        """Reagiere auf den Versuch, das Fenster zu schließen."""
        self.__log.debug("resetting")
        if self.__game.reset():
            self.__view.cancel_sleep()
            self.__log.info("switching to setup")
            self.__view.view_setup()
        else:
            self.__log.info("quitting")
            self.__view.quit()
