import logging

from controller import Controller

log = logging.getLogger("laedn")
log.setLevel(logging.DEBUG)
log_stdout = logging.StreamHandler()
log_stdout.setFormatter(logging.Formatter("%(levelname)s:%(name)s: %(message)s"))
log.addHandler(log_stdout)

controller = Controller(log)
controller.run()
