from typing import Callable, Tuple

from network import Action, Connection, Instruction, StopListening


class Client(object):
    def __init__(
        self,
        log,
        socket: "socket.socket",
        end_turn_cb: Callable[[int], None],
        begin_turn_cb: Callable[[int], None],
        show_die_cb: Callable[[int], None],
        home_vacated_cb: Callable[[int, int], None],
        home_filled_cb: Callable[[int, int, int], None],
        track_vacated_cb: Callable[[int], None],
        track_filled_cb: Callable[[int, int], None],
        dest_vacated_cb: Callable[[int, int], None],
        dest_filled_cb: Callable[[int, int, int], None],
        game_start_cb: Callable[[], None],
        game_end_cb: Callable[[int], None],
        process_view_cb: Callable[[], None],
        report_error_cb: Callable[[str], None],
    ):
        self.__log = log.getChild("client")
        self.__end_turn_cb = end_turn_cb
        self.__begin_turn_cb = begin_turn_cb
        self.__show_die_cb = show_die_cb
        self.__home_vacated_cb = home_vacated_cb
        self.__home_filled_cb = home_filled_cb
        self.__track_vacated_cb = track_vacated_cb
        self.__track_filled_cb = track_filled_cb
        self.__dest_vacated_cb = dest_vacated_cb
        self.__dest_filled_cb = dest_filled_cb
        self.__game_end_cb = game_end_cb
        self.__game_start_cb = game_start_cb
        self.__process_view_cb = process_view_cb
        self.__report_error_cb = report_error_cb
        self.__running = True
        self.__player_num = None
        self.__connection = Connection(self.__log, socket, process_view_cb)

    @property
    def player_num(self) -> int:
        """Die Nummer des lokalen Spielers."""
        if self.__player_num is None:
            self.__error(
                "Game did not yet start, cannot provide local player number.",
                stop_listening=False,
            )
        return self.__player_num

    def try_move_home(self, owner: int, field: int):
        """Fordere das Bewegen einer Figur an."""
        self.__connection.send("BBB", Action.MoveHome, owner, field)

    def try_move_track(self, field: int):
        """Fordere das Bewegen einer Figur an."""
        self.__connection.send("BB", Action.MoveTrack, field)

    def try_move_dest(self, owner: int, field: int):
        """Fordere das Bewegen einer Figur an."""
        self.__connection.send("BBB", Action.MoveDest, owner, field)

    def activate_die(self):
        """Fordere einen Würfelwurf an."""
        self.__connection.send("B", Action.RollDie)

    def run_client(self):
        """Starte den Client."""
        try:
            while self.__running:
                instr, = self.__recv("B")
                if instr == Instruction.GameStart.value:
                    self.__player_num = self.__player()
                    self.__log.debug("starting game")
                    self.__game_start_cb()
                elif instr == Instruction.HomeVacated.value:
                    self.__home_vacated_cb(self.__player(), self.__home())
                elif instr == Instruction.HomeFilled.value:
                    self.__home_filled_cb(
                        self.__player(), self.__home(), self.__player()
                    )
                elif instr == Instruction.TrackVacated.value:
                    self.__track_vacated_cb(self.__track())
                elif instr == Instruction.TrackFilled.value:
                    self.__track_filled_cb(self.__track(), self.__player())
                elif instr == Instruction.DestVacated.value:
                    self.__dest_vacated_cb(self.__player(), self.__dest())
                elif instr == Instruction.DestFilled.value:
                    self.__dest_filled_cb(
                        self.__player(), self.__dest(), self.__player()
                    )
                elif instr == Instruction.UpdateDie.value:
                    self.__show_die_cb(self.__pips())
                elif instr == Instruction.BeginTurn.value:
                    self.__begin_turn_cb(self.__player())
                elif instr == Instruction.EndTurn.value:
                    self.__end_turn_cb(self.__player())
                elif instr == Instruction.GameEnd.value:
                    self.__game_end_cb(self.__player())
                elif instr == Instruction.Error.value:
                    self.__error("server reported error")
                else:
                    self.__error("unknown instruction {}", instr)
        except StopListening:
            pass

    def quit(self):
        """Beende den Client."""
        self.__running = False
        self.__connection.close()

    def __recv(self, fmt: str) -> Tuple:
        """Empfange Werte nach dem struct `fmt`"""
        return self.__connection.recv(fmt, self.__checked_process)

    def __recv_bound(self, name: str, min: int, max: int) -> int:
        """Empfange ein byte in [`min`;`max`]."""
        recv, = self.__recv("B")
        if recv < min or recv > max:
            self.__error("received invalid {}: {} not in [{};{}]", name, recv, min, max)
        return recv

    def __pips(self) -> int:
        """Empfange eine Augenanzahl."""
        return self.__recv_bound("die", 1, 6)

    def __player(self) -> int:
        """Empfange einen Spieler."""
        return self.__recv_bound("player", 0, 3)

    def __track(self) -> int:
        """Empfange ein Feld."""
        return self.__recv_bound("track", 0, 39)

    def __home(self) -> int:
        """Empfange ein Haus-Feld."""
        return self.__recv_bound("home", 0, 4)

    def __dest(self) -> int:
        """Empfange ein Ziel-Feld."""
        return self.__recv_bound("dest", 0, 4)

    def __checked_process(self):
        """Update view.
        Breche ab, falls es zu beenden ist.
        """
        self.__process_view_cb()
        if not self.__running:
            raise StopListening()

    def __error(self, fmt: str, *data, stop_listening=True):
        """Behandle einen Fehler."""
        msg = ("Incompatible Server: " + fmt).format(*data)
        self.__log.error(msg)
        self.__report_error_cb(msg)
        self.__running = False
        if stop_listening:
            raise StopListening()
