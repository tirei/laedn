from typing import Callable, Optional


class Setup(object):
    """Halte die Spielkonfiguration."""

    def __init__(
        self,
        log,
        enable_setup_done_cb: Callable[[bool], None],
        enable_client_connect_cb: Callable[[bool], None],
    ):
        """Erstelle ein Setup Objekt."""
        self.__log = log.getChild("setup")
        self.__root_log = log
        self.__enable_setup_done_cb = enable_setup_done_cb
        self.__enable_client_connect_cb = enable_client_connect_cb
        self.__actors = [1] * 4
        self.__host = ""
        self.__port = None
        enable_setup_done_cb(True)
        enable_client_connect_cb(False)

    def set_player_mode(self, player: int, mode: int):
        """Merke die ID der Aktor-Klasse für einen Spieler für das nächste Spiel vor."""
        self.__actors[player] = mode
        self.__log.info("selected players: {}".format(self.__actors))
        self.__enable_setup_done_cb(any(self.__actors))

    def client_set_host(self, host: str):
        """Merke den Host (i.e. ipv4 addr oder DNS) für das nächste Spiel in Client Position vor."""
        self.__host = host
        self.__client_validate_addr()

    def client_set_port(self, port: str):
        """Merke den Port für das nächste Spiel in Client Position vor."""
        try:
            self.__port = int(port)
        except ValueError:
            self.__port = None
        self.__client_validate_addr()

    def __client_validate_addr(self):
        """Überprüfe Host und Port und de-/aktiviere den Bestätigungsknopf entsprechend."""
        self.__enable_client_connect_cb(self.__host and (self.__port is not None))

    def apply(
        self, model: "dispatch.ActorDispatch", start_server: bool
    ) -> Optional[bool]:
        """Konfiguriere `model` nach diesem Setup Objekt.

        Wenn `start_server` False ist, so starte einen Client. In diesem Fall gibt
        der Rückgabewert den Erfolg der Verbindung an.
        """
        self.__log.debug("applying setup to model")
        if start_server:
            model.init_server(self.__root_log, enumerate(self.__actors))
        else:
            return model.init_client(self.__root_log, self.__host, self.__port)
