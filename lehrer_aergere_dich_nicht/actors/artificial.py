from abc import abstractmethod
from typing import Iterator, List

import actor


class ArtificialActor(actor.Actor):
    """Ein künstlicher Akteur."""

    def __init__(self, *args):
        """Initialisiere einen künstlichen Akteur."""
        super().__init__(*args)
        self.__die = None
        self.__pieces = [[], [], [], []]
        self.__may_act = False

    @abstractmethod
    def _score(
        self, distance: int, pieces: List[int], enemies: List[List[int]]
    ) -> Iterator[int]:
        """Bewerte die möglichen Züge."""
        raise NotImplementedError()

    def end_turn(self, player: int):
        """Behandle ein Ereignis."""
        self.__may_act = False

    def begin_turn(self, player: int):
        """Behandle ein Ereignis."""
        if player == self._player_num:
            self.__may_act = True
            self._activate_die_cb()

    def home_vacated(self, owner: int, field: int):
        """Behandle ein Ereignis."""
        self.__pieces[self.__convert_player(owner)].remove(
            self.__convert_home(owner, field)
        )

    def home_filled(self, owner: int, field: int, piece: int):
        """Behandle ein Ereignis."""
        self.__pieces[self.__convert_player(piece)].append(
            self.__convert_home(owner, field)
        )

    def track_vacated(self, field: int):
        """Behandle ein Ereignis."""
        piece = self.__convert_track(field)
        for pieces in self.__pieces:
            if piece in pieces:
                pieces.remove(piece)

    def track_filled(self, field: int, piece: int):
        """Behandle ein Ereignis."""
        self.__pieces[self.__convert_player(piece)].append(self.__convert_track(field))

    def dest_vacated(self, owner: int, field: int):
        """Behandle ein Ereignis."""
        self.__pieces[self.__convert_player(owner)].remove(
            self.__convert_dest(owner, field)
        )

    def dest_filled(self, owner: int, field: int, piece: int):
        """Behandle ein Ereignis."""
        self.__pieces[self.__convert_player(piece)].append(
            self.__convert_dest(owner, field)
        )

    def update_die(self, pips: int):
        """Behandle ein Ereignis."""
        self.__die = pips

    def allow_move(self):
        """Behandle ein Ereignis."""
        if not self.__may_act:
            return
        self._log.debug(
            "scoring options from {}, d6={}".format(self.__pieces, self.__die)
        )
        for score, piece in sorted(
            zip(
                self._score(self.__die, self.__pieces[0], self.__pieces[1:]),
                self.__pieces[0],
            ),
            key=lambda score_piece: score_piece[0],
            reverse=True,
        ):
            if self.__may_act:
                self._log.debug("trying piece at {} (score={})".format(piece, score))
                if piece < 0:
                    self._try_move_home_cb(self._player_num, piece + 4)
                elif piece < 40:
                    self._try_move_track_cb((piece + self._player_num * 10) % 40)
                else:
                    self._try_move_dest_cb(self._player_num, piece - 40)
            else:
                self._log.debug("stop trying moves")
                break

    def _three_tries(self, player: int) -> bool:
        """Gebe zurück, ob `player` 3x würfeln darf."""
        pieces = self.__pieces[player]
        return all(
            piece < 0
            or (
                piece > 39
                and all(dest in pieces for dest in range(piece + 1, 44 + 4 * player))
            )
            for piece in pieces
        )

    def _is_start_field(self, field: int) -> bool:
        """Gebe zurück, ob `field` ein Startfeld ist."""
        return field >= 0 and field < 40 and field % 10 == 0

    def __convert_home(self, owner: int, field: int) -> int:
        """Convertiere ein Feld zur intenen Darstellung."""
        return field - 4 - 4 * self.__convert_player(owner)

    def __convert_track(self, field: int) -> int:
        """Convertiere ein Feld zur intenen Darstellung."""
        return (field - self._player_num * 10) % 40

    def __convert_dest(self, owner: int, field: int) -> int:
        """Convertiere ein Feld zur intenen Darstellung."""
        return field + 40 + 4 * self.__convert_player(owner)

    def __convert_player(self, player: int) -> int:
        """Convertiere einen Spieler zur intenen Darstellung."""
        return (player - self._player_num) % 4

    def __player_pieces(self, player: int) -> List[int]:
        """Gebe die Figuren des Spielers zurück."""
        return self.__pieces[self.__convert_player(player)]


class RunnerActor(ArtificialActor):
    def ready(self) -> bool:
        """Gibt zurück, ob dieser Akteur bereit ist."""
        return True

    def _score(
        self, distance: int, pieces: List[int], enemies: List[List[int]]
    ) -> Iterator[int]:
        """Bewerte die möglichen Züge."""
        for piece in pieces:
            dest = piece + distance
            if any(dest in enemy for enemy in enemies):
                self._log.debug("prefer {} to throw".format(piece))
                yield piece + 100
            elif (
                dest > 0
                and self._is_start_field(dest)
                and self._three_tries(dest // 10)
            ):
                self._log.debug(
                    "avoid {} moving to dangerous start field".format(piece)
                )
                yield piece - 100
            else:
                self._log.debug("score {} with field".format(piece))
                yield piece
