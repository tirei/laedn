import actor


class DisabledActor(actor.Actor):
    is_active = False

    def ready(self) -> bool:
        """Gibt zurück, ob dieser Akteur bereit ist."""
        self._log.debug("disabled")
        return True
