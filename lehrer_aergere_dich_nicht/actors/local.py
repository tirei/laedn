import actor


class LocalActor(actor.Actor):
    def ready(self) -> bool:
        """Gibt zurück, ob dieser Akteur bereit ist."""
        self._log.debug("ready")
        return True

    def click_roll_die(self):
        """Behandle ein Ereignis."""
        self._activate_die_cb()

    def click_move_home(self, owner: int, field: int):
        """Behandle ein Ereignis."""
        self._try_move_home_cb(owner, field)

    def click_move_track(self, field: int):
        """Behandle ein Ereignis."""
        self._try_move_track_cb(field)

    def click_move_dest(self, owner: int, field: int):
        """Behandle ein Ereignis."""
        self._try_move_dest_cb(owner, field)
