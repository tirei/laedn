import socket
from typing import Callable, Optional, Tuple

import actor
import network
from network import Action, Instruction, StopListening


class Listener(object):
    """Akzeptiere Verbindungen."""

    def __init__(self, log, process: Callable[[], None]):
        """Erstelle einen Listener."""
        self.__log = log
        self.__process = process
        self.__socket = socket.socket()
        self.__socket.bind(("", 0))
        self.__socket.setblocking(False)
        self.__socket.listen(1)
        self.__log.info("listening on port {}".format(self.port))

    def accept(self) -> Optional[network.Connection]:
        """Versuche eine Connection zu akzeptieren."""
        try:
            connection, addr = self.__socket.accept()
        except BlockingIOError:
            return
        self.__log.info(
            "connection accepted on port {} from {}".format(self.port, addr)
        )
        self.__socket.close()
        return network.Connection(self.__log, connection, self.__process)

    @property
    def port(self) -> int:
        """Der Port, auf dem eine Verbindung erwartet wird."""
        return self.__socket.getsockname()[1]


class RemoteActor(actor.Actor):
    """Das Kontrollieren eines Spielers durch einen Client."""

    def __init__(self, *args):
        """Erstelle einen RemoteActor."""
        super().__init__(*args)
        self.__may_act = False
        self.__may_roll_die = False
        self.__connection = None
        self.__listener = Listener(self._log, self._process_view_cb)
        self._show_connection_info_cb(self._player_num, self.__listener.port)

    def ready(self) -> bool:
        """Gibt zurück, ob dieser Akteur bereit ist."""
        self.__connection = self.__listener.accept()
        return self.__connection is not None

    def quit(self):
        """Beende den Akteur."""
        self.__may_act = False
        if self.__connection:
            self.__connection.close()

    def end_turn(self, player: int):
        """Behandle ein Ereignis."""
        self.__may_act = False
        self.__connection.send("BB", Instruction.EndTurn, player)

    def begin_turn(self, player: int):
        """Behandle ein Ereignis."""
        self.__connection.send("BB", Instruction.BeginTurn, player)
        if player == self._player_num:
            self.__may_act = True
            self.__may_roll_die = True
            self.__connection.consume()
            recv = lambda fmt: self.__connection.recv(fmt, self.__checked_process)
            while self.__may_act:
                try:
                    action, = recv("B")
                    if action == Action.MoveTrack.value:
                        field, = recv("B")
                        if field < 40:
                            self._try_move_track_cb(field)
                        else:
                            self.__error("invalid field track #{} received", field)
                    elif action <= Action.MoveDest.value:
                        owner, field = recv("BB")
                        if owner < 4 and field < 4:
                            (
                                self._try_move_home_cb
                                if action == Action.MoveHome.value
                                else self._try_move_dest_cb
                            )(owner, field)
                        else:
                            self.__error(
                                "invalid field {} owner{}#{}",
                                "home" if action == Action.MoveHome.value else "dest",
                                owner,
                                field,
                            )
                    elif action == Action.RollDie.value:
                        if self.__may_roll_die:
                            self.__may_roll_die = False
                            self._activate_die_cb()
                    elif action == Action.Error.value:
                        self.__error("client reported error")
                    else:
                        self.__error("invalid action '{}' received", action)
                except StopListening:
                    self._log.debug("interrupted recv")

    def home_vacated(self, owner: int, field: int):
        """Behandle ein Ereignis."""
        self.__connection.send("BBB", Instruction.HomeVacated, owner, field)

    def home_filled(self, owner: int, field: int, piece: int):
        """Behandle ein Ereignis."""
        self.__connection.send("BBBB", Instruction.HomeFilled, owner, field, piece)

    def track_vacated(self, field: int):
        """Behandle ein Ereignis."""
        self.__connection.send("BB", Instruction.TrackVacated, field)

    def track_filled(self, field: int, piece: int):
        """Behandle ein Ereignis."""
        self.__connection.send("BBB", Instruction.TrackFilled, field, piece)

    def dest_vacated(self, owner: int, field: int):
        """Behandle ein Ereignis."""
        self.__connection.send("BBB", Instruction.DestVacated, owner, field)

    def dest_filled(self, owner: int, field: int, piece: int):
        """Behandle ein Ereignis."""
        self.__connection.send("BBBB", Instruction.DestFilled, owner, field, piece)

    def game_end(self, winner: int):
        """Behandle ein Ereignis."""
        self.__connection.send("BB", Instruction.GameEnd, winner)

    def game_start(self):
        """Behandle ein Ereignis."""
        self.__connection.send("BB", Instruction.GameStart, self._player_num)

    def update_die(self, pips: int):
        """Behandle ein Ereignis."""
        self.__connection.send("BB", Instruction.UpdateDie, pips)

    def __checked_process(self):
        """Update View und breche ab, falls notwendig."""
        self._process_view_cb()
        if not self.__may_act:
            raise StopListening()

    def __error(self, fmt, *data):
        """Behandle einen Fehler."""
        self._log.error(fmt.format(*data))
        self.__connection.send("B", Instruction.Error)
