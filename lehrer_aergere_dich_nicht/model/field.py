from itertools import chain
from typing import Optional


class Field(object):
    """Ein Feld."""

    def __init__(self, field_id: int, log, name: str, vacate_obsv, fill_obsv):
        """Erstelle ein Feld."""
        self.__id = field_id
        self._log = log.getChild("{}#{}".format(name, self.id))
        self.__vacated_obsv = vacate_obsv
        self.__filled_obsv = fill_obsv
        self.__piece = None

    def vacate(self):
        """Leere das Feld."""
        self._log.debug("vacating")
        self.__piece = None
        self.__vacated_obsv(self.id)

    def fill(self, piece: "Piece"):
        """Fülle das Feld."""
        self._log.debug("filling")
        if self.piece:
            self._log.critical("filling non-empty field!")
        self.__piece = piece
        self.__filled_obsv(self.id, piece.owner.id)

    @property
    def piece(self) -> Optional["Piece"]:
        """Der besetzende Spielstein."""
        return self.__piece

    @property
    def occupant(self) -> Optional["Player"]:
        """Der besetzende Spieler."""
        return self.piece and self.piece.owner

    @property
    def id(self):
        """Die Feld-ID."""
        return self.__id

    def is_packed(self) -> bool:
        """Sind die Figuren hier aufgerückt."""
        return self.piece is not None

    def try_dest(self, player: "Player", distance: int) -> Optional["Field"]:
        """Berechne das mögliche Ziel einer Figur."""
        self._log.debug("cannot move from final field")
        return None

    def _skip(self, distance: int, player: "Player") -> Optional["Field"]:
        """Das `distance`-nächste Feld."""
        if distance == 0:
            return self


class ConnectedField(Field):
    """Ein Feld mit einem nächsten Feld."""

    def __init__(self, *args):
        """Erstelle ein Feld."""
        super().__init__(*args)
        self.__next_field = None

    def connect_next(self, next_field: Field):
        """Verbinde zum nächsten Feld."""
        self.__next_field = next_field

    def try_dest(self, player: "Player", distance: int) -> Optional[Field]:
        """Berechne das mögliche Ziel einer Figur."""
        if (
            distance == 6
            and self is not player.start_field
            and self not in player.home
            and any(home.piece for home in player.home)
            and any(
                field.try_dest(player, distance)
                for field in chain((player.start_field,), player.home)
                if field.piece
            )
        ):
            self._log.debug("player has to move out of home or vacate start field")
            return None
        if (
            player.start_field.occupant is player
            and player.start_field is not self
            and any(home.piece for home in player.home)
            and player.start_field.try_dest(player, distance) is not None
        ):
            self._log.debug("player has to vacate start field")
            return None
        dest = self._skip(distance, player)
        if not dest:
            self._log.debug("moving {} from here would overshoot".format(distance))
            return None
        if dest.occupant is player:
            self._log.debug("player cannot throw his own piece")
            return None
        self._log.debug("can throw" if dest.piece else "can move")
        return dest

    def is_packed(self) -> bool:
        """Sind die Figuren hier aufgerückt."""
        return (
            super().is_packed()
            and self.__next_field.occupant is self.occupant
            and self.__next_field.is_packed()
        )

    def _skip(self, distance: int, player: "Player") -> Optional[Field]:
        """Das `distance`-nächste Feld."""
        return super()._skip(distance, player) or self.__next_field._skip(
            distance - 1, player
        )


class JunctionField(ConnectedField):
    def __init__(self, *args):
        super().__init__(*args)
        self.__player = None

    def connect_player(self, player: "Player"):
        self._log.debug("connect {}'s junction to his dest".format(player))
        self.__player = player

    def _skip(self, distance: int, player: "Player") -> Optional[Field]:
        if player is self.__player:
            self._log.debug("{} has to use his junction".format(player))
            return player.dest[0]._skip(distance - 1, player)
        self._log.debug("{} skips {}'s junction".format(player, self.__player))
        return super()._skip(distance, player)


class HomeField(ConnectedField):
    """Ein Haus Feld."""

    def __init__(self, field_id: int, log, start_field: Field, *rest):
        """Erstelle ein Haus Feld."""
        super().__init__(field_id, log, "home", *rest)
        self.connect_next(start_field)

    def try_dest(self, player: "Player", distance: int) -> Optional[Field]:
        """Berechne das mögliche Ziel einer Figur."""
        if distance == 6:
            return super().try_dest(player, distance)
        self._log.debug("need a 6 to move out of home")

    def _skip(self, distance: int, player: "Player") -> Optional[Field]:
        """Das `distance`-nächste Feld."""
        if distance == 6:
            return super()._skip(1, player)
