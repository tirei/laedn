import itertools
from typing import Callable, Iterator, Tuple

from model.die import Die
from model.field import ConnectedField, Field, JunctionField
from model.player import Player


class Game(object):
    """Ein Spiel Lehrer-ärgere-dich-nicht."""

    def __init__(
        self,
        log,
        end_turn_obsv: Callable[[int], None],
        begin_turn_obsv: Callable[[int], None],
        home_vacated_obsv: Callable[[int, int], None],
        home_filled_obsv: Callable[[int, int, int], None],
        track_vacated_obsv: Callable[[int], None],
        track_filled_obsv: Callable[[int, int], None],
        dest_vacated_obsv: Callable[[int, int], None],
        dest_filled_obsv: Callable[[int, int, int], None],
        game_end_obsv: Callable[[int], None],
    ):
        """Erstelle ein Spiel Objekt."""
        self.__log = log.getChild("game")
        self.__end_turn_obsv = end_turn_obsv
        self.__begin_turn_obsv = begin_turn_obsv
        self.__home_vacated_obsv = home_vacated_obsv
        self.__home_filled_obsv = home_filled_obsv
        self.__track_vacated_obsv = track_vacated_obsv
        self.__track_filled_obsv = track_filled_obsv
        self.__dest_vacated_obsv = dest_vacated_obsv
        self.__dest_filled_obsv = dest_filled_obsv
        self.__game_end_obsv = game_end_obsv

        self.__die = Die(self.__log)
        self.__players = []
        self.__all_players = []
        self.__active_player_idx = 0
        self.__init_track()

        self.__tries = 0
        self.__allow_move = False

    def __init_track(self):
        """Erstelle die Straße."""
        obsv = (self.__track_vacated_obsv, self.__track_filled_obsv)
        self.__track = [ConnectedField(0, self.__log, "track", *obsv)]
        for field_id in range(1, 40):
            Field = JunctionField if field_id % 10 == 9 else ConnectedField
            field = Field(field_id, self.__log, "track", *obsv)
            self.__track[-1].connect_next(field)
            self.__track.append(field)
        self.__track[-1].connect_next(self.__track[0])

    def set_players(self, players: Iterator[Tuple[int, bool]]):
        """Initialisiere die Spieler."""
        for num, plays in players:
            player = Player(
                self.__log,
                num,
                self.__track[num * 10],
                (self.__home_vacated_obsv, self.__home_filled_obsv),
                (self.__dest_vacated_obsv, self.__dest_filled_obsv),
            )
            self.__track[(num * 10 - 1) % 40].connect_player(player)
            self.__all_players.append(player)
            if plays:
                self.__players.append(player)
        self.__begin_turn()

    def reset(self):
        """Setze das Spiel zurueck."""
        self.__allow_move = False
        for player in self.__players:
            player.reset_pieces()
        self.__players.clear()
        self.__all_players.clear()
        self.__active_player_idx = 0
        self.__tries = 0

    def roll_die(self) -> int:
        """Werfe den Würfel und gebe die Augenzahl zurück."""
        self.__die.roll()
        return self.__die.get()

    def try_move_home(self, owner_id: int, field_id: int):
        """Versuche eine Figur zu bewegen."""
        self.__log.debug(
            "{} tries to move from home #{} of {}".format(
                self.__active_player, field_id, self.__player(owner_id)
            )
        )
        self.__try_move(self.__player(owner_id).home[field_id])

    def try_move_track(self, field_id: int):
        """Versuche eine Figur zu bewegen."""
        self.__log.debug(
            "{} tries to move from track #{}".format(self.__active_player, field_id)
        )
        self.__try_move(self.__track[field_id])

    def try_move_dest(self, owner_id: int, field_id: int):
        """Versuche eine Figur zu bewegen."""
        self.__log.debug(
            "{} tries to move from destination #{} of {}".format(
                self.__active_player, field_id, self.__player(owner_id)
            )
        )
        self.__try_move(self.__player(owner_id).dest[field_id])

    def allow_move(self):
        """Erlaube einen Zug durchzuführen."""
        player = self.__active_player
        player.calc_dests(self.__die.get())
        self.__allow_move = player.can_move()
        if not self.__allow_move:
            self.__log.info("{} can't move".format(str(player)))
            self.__end_move()

    def __try_move(self, src: Field):
        """Versuche eine Figur zu bewegen."""
        player = self.__active_player
        if src.occupant is not player:
            self.__log.info(
                "{} can't move {}'s pieces".format(player, src.occupant or "nobody")
            )
            return
        dest = src.piece.dest
        if self.__allow_move and dest:
            self.__log.info("moving!")
            if dest.piece:
                self.__log.info("throwing {}'s piece".format(dest.occupant))
                dest.piece.move_to_home()
            elif player.can_throw():
                self.__log.info("player failed to throw")
                player.reset_throwers()
            src.piece.move_to_dest()

            if player.has_won():
                self.__allow_move = False
                self.__log.info("{} has won!!!".format(player))
                self.__game_end_obsv(player.id)
            else:
                self.__end_move()

    @property
    def __active_player(self) -> Player:
        """Der Spieler, der dran ist."""
        return self.__players[self.__active_player_idx]

    def __end_move(self):
        """Beende einen Zug."""
        player = self.__active_player
        self.__allow_move = False
        self.__end_turn_obsv(player.id)
        if self.__die.get() == 6:
            self.__log.info("{} rolled a 6, may move again".format(player))
            self.__tries = 0
            self.__begin_move()
        elif self.__tries == 0:
            self.__log.debug("{} may not try again, next player".format(player))
            self.__next_player()
        else:
            self.__log.debug("{} has {} tries left".format(player, self.__tries))
            self.__tries -= 1
            self.__begin_move()

    def __next_player(self):
        """Gehe zum nächsten Spieler über."""
        self.__active_player_idx = (self.__active_player_idx + 1) % len(self.__players)
        self.__begin_turn()

    def __player(self, num: int) -> Player:
        """Wandle eine Spieler ID in den Spieler um."""
        return self.__all_players[num]

    def __begin_turn(self):
        """Aktiviere den Spieler."""
        player = self.__active_player
        self.__tries = (
            2
            if (
                all(field.occupant is not player for field in self.__track)
                and all(
                    field.is_packed()
                    for field in player.dest
                    if field.occupant is player
                )
            )
            else 0
        )
        self.__begin_move()

    def __begin_move(self):
        """Beginne einen Zug."""
        self.__begin_turn_obsv(self.__active_player.id)
