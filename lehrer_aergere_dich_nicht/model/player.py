from functools import partial
from typing import List, Optional

from model.field import ConnectedField, Field, HomeField


class Player(object):
    """Ein Spieler im Sinne der Spielregeln (i.e. kein Akteur)."""

    def __init__(
        self, log, player_id: int, start_field: ConnectedField, home_obsv, dest_obsv
    ):
        """Erstelle einen Spieler."""
        self.__id = player_id
        self.__name = "Player {}".format(["Red", "Blue", "Green", "Yellow"][self.id])
        self.__log = log.getChild("p{}[{}]".format(self.__id, str(self)))
        self.__log.debug("creating player")
        self.__start_field = start_field
        self.__init_home(self.__wrap_obsv(home_obsv))
        self.__init_dest(self.__wrap_obsv(dest_obsv))

    def __init_home(self, obsv):
        """Erstelle das Haus inkl. Spielsteine."""
        self.__home = [
            HomeField(field_id, self.__log, self.start_field, *obsv)
            for field_id in range(4)
        ]
        self.__pieces = [
            Piece(self.__log, piece_id, self, home)
            for piece_id, home in enumerate(self.__home)
        ]

    def __init_dest(self, obsv):
        """Erstelle das Ziel."""
        self.__dest = [ConnectedField(0, self.__log, "dest", *obsv)]
        for field_id in range(1, 3):
            field = ConnectedField(field_id, self.__log, "dest", *obsv)
            self.__dest[-1].connect_next(field)
            self.__dest.append(field)
        last = Field(3, self.__log, "dest", *obsv)
        self.__dest[-1].connect_next(last)
        self.__dest.append(last)

    @property
    def id(self) -> int:
        """Die Spieler ID"""
        return self.__id

    @property
    def start_field(self) -> ConnectedField:
        """Das Start Feld des Spielers."""
        return self.__start_field

    @property
    def home(self) -> List[HomeField]:
        """Die Haus Felder."""
        return self.__home

    @property
    def dest(self) -> List[Field]:
        """Die Ziel Felder."""
        return self.__dest

    def calc_dests(self, distance: int):
        """Berechne alle möglichen Ziele."""
        self.__log.debug("calculating destinations for distance {}".format(distance))
        for piece in self.__pieces:
            piece.calc_dest(distance)

    def can_move(self) -> bool:
        """Gebe zurück, ob dieser Spieler einen Zug durchführen kann."""
        return any(piece.dest for piece in self.__pieces)

    def can_throw(self) -> bool:
        """Gebe zurück, ob dieser Spieler werfen kann."""
        return any(piece.can_throw() for piece in self.__pieces)

    def has_won(self) -> bool:
        """Gebe zurück, ob dieser Spieler gewonnen hat."""
        return all(dest.occupant is self for dest in self.dest)

    def reset_pieces(self):
        """Stelle alle Spielfiguren zurück."""
        for piece in self.__pieces:
            piece.move_to_home()

    def reset_throwers(self):
        """Stelle Spielfiguren zurück, die hätten schlagen können."""
        for piece in self.__pieces:
            if piece.can_throw():
                piece.move_to_home()

    def __wrap_obsv(self, obsv):
        """Vermerke die Spieler ID für die Beobachter."""
        return (partial(obsv[0], self.id), partial(obsv[1], self.id))

    def __str__(self) -> str:
        """Der Spieler name."""
        return self.__name


class Piece(object):
    """Ein Spielstein"""

    def __init__(self, log, piece_id: int, owner: Player, location: HomeField):
        """Erstelle ein Piece."""
        self.__id = piece_id
        self.__log = log.getChild(str(self))
        self.__owner = owner
        self.__location = location
        self.__location.fill(self)
        self.__dest = None

    @property
    def owner(self) -> Player:
        """Der Besitzer."""
        return self.__owner

    @property
    def dest(self) -> Optional[Field]:
        """Das mögliche Ziel."""
        return self.__dest

    def calc_dest(self, distance: int):
        """Berechne das mögliche Ziel."""
        self.__dest = self.__location.try_dest(self.owner, distance)

    def move_to_home(self):
        """Setze diese Figur zurück."""
        for home in self.__owner.home:
            if not home.piece:
                self.__move_to(home)
                return
            if home.piece is self:
                return
        self.__log.critical(
            "There is no available home field for {} in {}'s home {}!".format(
                self, self.owner, self.owner.home
            )
        )
        raise Exception("no free home field")

    def move_to_dest(self):
        """Bewege diese Figur."""
        self.__move_to(self.__dest)

    def can_throw(self) -> bool:
        """Gebe zurück, ob diese Figur schlagen kann."""
        return bool(self.dest and self.dest.piece)

    def __move_to(self, dest: Field):
        """Bewege diese Figur."""
        self.__log.debug("moving to {}".format(dest))
        self.__location.vacate()
        self.__location = dest
        self.__location.fill(self)

    def __str__(self) -> str:
        """Bennenne diese Figur."""
        return "piece{}".format(self.__id)
