import random


class Die(object):
    """Ein Würfel."""

    def __init__(self, log):
        """Erstelle einen Würfel."""
        self.__log = log.getChild("die")
        self.__pips = None

    def roll(self):
        """Werfe den Würfel."""
        self.__pips = random.randint(1, 6)
        self.__log.debug("rolled {}".format(self))

    def get(self) -> int:
        """Lese den Würfel."""
        return self.__pips

    def __str__(self):
        """Formatiere `self` als String."""
        return "d6={}".format(self.get())
