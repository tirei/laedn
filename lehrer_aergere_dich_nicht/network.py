import enum
import socket
import struct
from typing import Callable, Tuple


class Connection(object):
    """Ein Socket-Wrapper, der regelmäßig die View updaten kann."""

    def __init__(self, log, socket: socket.socket, process: Callable[[], None]):
        self.__log = log
        self.__process = process
        self.__socket = socket
        self.__socket.setblocking(False)

    def recv(self, fmt: str, process=None) -> Tuple:
        """Empfange Werte nach dem struct `fmt`."""
        process = process or self.__process
        data = b""
        fmtc = struct.Struct("!" + fmt)
        while len(data) < fmtc.size:
            try:
                data += self.__socket.recv(fmtc.size - len(data))
            except BlockingIOError:
                pass
            process()
        values = fmtc.unpack(data)
        self.__log.debug("received '{}' -> {}".format(fmt, values))
        return values

    def send(self, fmt: str, *values):
        """Sende Werte nach dem struct `fmt`."""
        self.__log.debug("sending '{}' <- {}".format(fmt, values))
        data = struct.pack("!" + fmt, *values)
        while data:
            try:
                sent = self.__socket.send(data)
            except BlockingIOError:
                pass
            data = data[sent:]
            self.__process()

    def consume(self):
        """Verbrauche alle wartenden Bytes."""
        recv = lambda: self.__socket.recv(1024)
        try:
            data = recv()
            while len(data) > 0:
                self.__log.debug("skipping received data: {}".format(data))
                self.__process()
                data = recv()
        except BlockingIOError:
            pass

    def close(self):
        """Schließe die Verbindung."""
        self.__log.debug("closing connection")
        try:
            self.__socket.shutdown(socket.SHUT_RDWR)
            self.__socket.close()
        except OSError:
            self.__log.debug("connection already closed")


class Instruction(enum.Enum):
    """Server -> Client Nachrichten."""

    GameStart = 0
    HomeVacated = 1
    HomeFilled = 2
    TrackVacated = 3
    TrackFilled = 4
    DestVacated = 5
    DestFilled = 6
    UpdateDie = 7
    BeginTurn = 8
    EndTurn = 9
    GameEnd = 10
    Error = 255

    def __index__(self) -> int:
        """Wandle `self` in eine Ganzzahl um."""
        return self.value


class Action(enum.Enum):
    """Client -> Server Nachrichten."""

    MoveHome = 0
    MoveTrack = 1
    MoveDest = 2
    RollDie = 3
    Error = 255

    def __index__(self) -> int:
        """Wandle `self` in eine Ganzzahl um."""
        return self.value


class StopListening(Exception):
    pass
