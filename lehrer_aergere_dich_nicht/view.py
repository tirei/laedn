import os
from functools import partial
from tkinter import *


def image(name: str) -> PhotoImage:
    path = os.path.join(os.path.dirname(__file__), "img", name)
    return PhotoImage(file=path)


class GUI(object):
    __dice = ["⚀", "⚁", "⚂", "⚃", "⚄", "⚅"]
    __choices = [
        "Deaktiviert",
        "Lokaler Spieler",
        "Globaler Spieler",
        "Computerspieler",
    ]

    def __init__(
        self,
        log,
        set_player_mode_cb,
        setup_done_cb,
        client_set_host_cb,
        client_set_port_cb,
        client_connect_cb,
        roll_die_cb,
        move_home_cb,
        move_track_cb,
        move_dest_cb,
        close_window_cb,
    ):
        """Inizialisierung und erstellen der GUI"""
        self.__fenster = Tk()
        self.__fenster.title("Lehrer ärgere dich nicht")

        self.__log = log.getChild("gui")
        self.__set_player_mode_cb = set_player_mode_cb
        self.__setup_done_cb = setup_done_cb
        self.__client_set_host_cb = client_set_host_cb
        self.__client_set_port_cb = client_set_port_cb
        self.__client_connect_cb = client_connect_cb
        self.__roll_die_cb = roll_die_cb
        self.__move__home__cb = move_home_cb
        self.__move_track_cb = move_track_cb
        self.__move_dest_cb = move_dest_cb
        self.__fenster.protocol("WM_DELETE_WINDOW", close_window_cb)

        self.__after_id = ""

        self.__listHomes = []
        self.__listBackHomes = []
        self.__listTracks = []
        self.__listStarts = []
        self.__listDests = []
        self.__listBackDest = []
        self.__listBackConnect = []
        self.__listPieces = []
        self.__listVariablenDropdown = []
        self.__listDropdown = []
        self.__listFramesSpielerLobby = []
        self.__listLabelsPorts = []
        self.__listFramesConnections = []
        self.__listImagesArrows = []
        self.__listLabelsArrows = []

        self.__red_piece = image("red_piece.png")
        self.__listPieces.append(self.__red_piece)
        self.__blue_piece = image("blue_piece.png")
        self.__listPieces.append(self.__blue_piece)
        self.__green_piece = image("green_piece.png")
        self.__listPieces.append(self.__green_piece)
        self.__yellow_piece = image("yellow_piece.png")
        self.__listPieces.append(self.__yellow_piece)

        self.__arrow_right = image("arrow_right.png")
        self.__listImagesArrows.append(self.__arrow_right)
        self.__arrow_down = image("arrow_down.png")
        self.__listImagesArrows.append(self.__arrow_down)
        self.__arrow_left = image("arrow_left.png")
        self.__listImagesArrows.append(self.__arrow_left)
        self.__arrow_up = image("arrow_up.png")
        self.__listImagesArrows.append(self.__arrow_up)

        self.__connected = image("connected.png")
        self.__not_connected = image("not_connected.png")

        # -------------------- Setup-GUI

        # Erstellung des Frames für den Background
        self.__frameBackgroundSetup = Frame(master=self.__fenster, background="grey")

        # Erstellung des Frames für die Überschrift
        self.__frameUeberschriftSetup = Frame(
            master=self.__frameBackgroundSetup, background="#4c2f26"
        )

        # Erstellung des Labels für die Überschrift
        self.__labelUeberschriftSetup = Label(
            master=self.__frameUeberschriftSetup,
            background="#399d2c",
            foreground="black",
            text="Lehrer ärgere \n dich nicht",
            font="Algerian 20",
        )

        # Erstellung des Frames für das lokale Spiel
        self.__frameLocal = Frame(
            master=self.__frameBackgroundSetup, background="#4c2f26"
        )

        # Erstellung des Frames für die Überschrift des lokalen Spiels
        self.__frameUeberschriftLocal = Frame(
            master=self.__frameLocal, background="grey"
        )

        # Erstellung des Labels für die Überschrift
        self.__labelUeberschriftLocal = Label(
            master=self.__frameUeberschriftLocal,
            background="#399d2c",
            foreground="black",
            text="Lokaels Spiel",
            font="Broadway 16",
        )

        # Erstellung der Variablen für die Dropdowns
        for i in range(4):
            variableDropdown = StringVar(self.__frameLocal)
            variableDropdown.set("Lokaler Spieler")
            self.__listVariablenDropdown.append(variableDropdown)

        # Erstellung des Dropdowns für die Spieler
        for i in range(4):
            dropdown = OptionMenu(
                self.__frameLocal, self.__listVariablenDropdown[i], *self.__choices
            )
            self.__listDropdown.append(dropdown)

        # Verfolgen der Variablen für die Dropdowns
        for i in range(4):

            def dropdownchange(player, a, b, c):
                """Ruft die übergebene Funktion auf, wenn sich die Auswahl in dem OptionMenu ändert"""
                self.__set_player_mode_cb(
                    player,
                    self.__choices.index(self.__listVariablenDropdown[player].get()),
                )

            self.__listVariablenDropdown[i].trace("w", partial(dropdownchange, i))

        # Erstellung des Buttons um das lokale Spiel zu starten
        self.__buttonStartLocal = Button(
            master=self.__frameLocal,
            background="white",
            foreground="black",
            command=self.__setup_done_cb,
            font="Broadway 12",
        )

        # Erstellung des Frames für das globale Spiel
        self.__frameGlobalesSpiel = Frame(
            master=self.__frameBackgroundSetup, background="#4c2f26"
        )

        # Erstellung des Frames für die Überschrift des globales Spiels
        self.__frameUeberschriftGlobal = Frame(
            self.__frameGlobalesSpiel, background="grey"
        )

        # Erstellung des Labels für die Überschrift des globalen Spiels
        self.__labelUeberschriftGlobal = Label(
            master=self.__frameUeberschriftGlobal,
            background="#399d2c",
            foreground="black",
            text="Globales Spiel",
            font="Broadway 16",
        )

        # Erstellung der Variable für das Entrys für die IP
        self.__variableEntryIP = StringVar(self.__frameGlobalesSpiel)
        self.__variableEntryIP.set = ""

        # Verfolgen der Variable für die IP
        def entryIPchange(*args):
            """Ruft die übergebene Funktion auf, wenn sich der Eintrag in dem Entry für die IP ändert"""
            self.__client_set_host_cb(self.__variableEntryIP.get())

        self.__variableEntryIP.trace("w", entryIPchange)

        # Label für das Entry für die IP für das globale Spiel
        self.__labelIPentry = Label(master=self.__frameGlobalesSpiel, text="IP:")

        # Erstellung des Entrys für die IP für das globale Spiel
        self.__entryIP = Entry(
            master=self.__frameGlobalesSpiel, textvariable=self.__variableEntryIP
        )

        # Erstellung der Variable für das Entrys für die IP
        self.__variableEntryPort = StringVar(self.__frameGlobalesSpiel)
        self.__variableEntryPort.set = ""

        # Verfolgen der Variable für die IP
        def entryPortchange(*args):
            """Ruft die übergebene Funktion auf, wenn sich der Eintrag in dem Entry für den Port ändert"""
            self.__client_set_port_cb(self.__variableEntryPort.get())

        self.__variableEntryPort.trace("w", entryPortchange)

        # Label für das Entry für den Port für das globale Spiel
        self.__labelPortentry = Label(master=self.__frameGlobalesSpiel, text="Port:")

        # Erstellung des Entrys für den Port für das globale Spiel
        self.__entryPort = Entry(
            master=self.__frameGlobalesSpiel, textvariable=self.__variableEntryPort
        )

        # Erstellung des Buttons um das globale Spiel zu starten
        self.__buttonGlobalesSpiel = Button(
            master=self.__frameGlobalesSpiel,
            background="white",
            foreground="black",
            command=self.__client_connect_cb,
            font="Broadway 12",
        )

        # -------------------- Lobby-GUI (waiting)

        #  Erstellung des Frames für den Background
        self.__backgroundLobbyGUI = Frame(master=self.__fenster, background="grey")

        # Erstellung die Frames für die Spieler
        for i in range(4):
            frameSpieler = Frame(
                master=self.__backgroundLobbyGUI,
                background=["red", "blue", "green", "yellow"][i],
            )
            self.__listFramesSpielerLobby.append(frameSpieler)

        # Erstellung die Labels für die Ports
        for i in range(4):
            labelPort = Label(
                master=self.__listFramesSpielerLobby[i], text="Spieler " + str(i)
            )
            self.__listLabelsPorts.append(labelPort)

        # Erstellung der Frames für die Bilder für die Verbindung
        for i in range(4):
            frameConnection = Label(
                master=self.__listFramesSpielerLobby[i], image=self.__connected
            )
            self.__listFramesConnections.append(frameConnection)

        # -------------------- Spielfeld-GUI

        #  Erstellung des Frames für den Background
        self.__frameBackgroundGame = Frame(master=self.__fenster, background="grey")

        # Erstellung der Frames für die Homes
        for i in range(4):
            frameHomeGame = Frame(
                master=self.__frameBackgroundGame,
                background=["red", "blue", "green", "yellow"][i],
            )
            self.__listBackHomes.append(frameHomeGame)

        # Erstellung der Buttons für die Homes
        for i in range(4):
            zwischenListe = []
            for a in range(4):
                buttonHomeGames = Button(
                    master=self.__listBackHomes[i],
                    background=["red", "blue", "green", "yellow"][i],
                    command=partial(self.__move__home__cb, i, a),
                )
                zwischenListe.append(buttonHomeGames)
            self.__listHomes.append(zwischenListe)

        # Erstellung des Frames für die Verbingung der Buttons
        for i in range(12):
            frameVerbindungGame = Frame(
                master=self.__frameBackgroundGame, background="black"
            )
            self.__listBackConnect.append(frameVerbindungGame)

        # Erstellung der Labels für die Pfeile
        for i in range(4):
            labelArrow = Label(
                master=self.__frameBackgroundGame,
                background="grey",
                image=self.__listImagesArrows[i],
            )
            self.__listLabelsArrows.append(labelArrow)

        # Erstellung der Buttons für den Track
        for i in range(40):
            if i == 0:
                buttonTrackGame = Button(
                    master=self.__frameBackgroundGame,
                    background="red",
                    command=partial(self.__move_track_cb, i),
                )
                self.__listStarts.append(buttonTrackGame)

            elif i == 10:
                buttonTrackGame = Button(
                    master=self.__frameBackgroundGame,
                    background="blue",
                    command=partial(self.__move_track_cb, i),
                )
                self.__listStarts.append(buttonTrackGame)

            elif i == 20:
                buttonTrackGame = Button(
                    master=self.__frameBackgroundGame,
                    background="green",
                    command=partial(self.__move_track_cb, i),
                )
                self.__listStarts.append(buttonTrackGame)

            elif i == 30:
                buttonTrackGame = Button(
                    master=self.__frameBackgroundGame,
                    background="yellow",
                    command=partial(self.__move_track_cb, i),
                )
                self.__listStarts.append(buttonTrackGame)

            else:
                buttonTrackGame = Button(
                    master=self.__frameBackgroundGame,
                    background="white",
                    command=partial(self.__move_track_cb, i),
                )

            self.__listTracks.append(buttonTrackGame)

        # Erstellung der Frames für die Destinations
        for i in range(4):
            frameDestGame = Frame(master=self.__frameBackgroundGame, background="grey")
            self.__listBackDest.append(frameDestGame)

        # Buttons für die Destinations
        for i in range(4):
            zwischenListe = []
            for a in range(4):
                buttonDestGame = Button(
                    master=self.__listBackDest[i],
                    background=["red", "blue", "green", "yellow"][i],
                    command=partial(self.__move_dest_cb, i, a),
                )
                zwischenListe.append(buttonDestGame)
            self.__listDests.append(zwischenListe)

        # Erstellung der Buttons für den Würfel
        self.__buttonWuerfelGame = Button(
            master=self.__frameBackgroundGame,
            background="white",
            foreground="black",
            font="Arial 44",
            text="⚅",
            command=self.__roll_die_cb,
        )

        # -------------------- Error-GUI
        # Erstellung des Hintergrunds für die Fehlermeldung
        self.__frameBackgroundError = Frame(master=self.__fenster, background="grey")

        # Erstellung des Labels für die Fehlermeldung
        self.__labelError = Label(
            master=self.__frameBackgroundError, background="grey", text=""
        )

    def view_setup(self):
        """Aufrufen des Setups"""
        self.__frameBackgroundSetup.place_forget()
        self.__backgroundLobbyGUI.place_forget()
        self.__frameBackgroundGame.place_forget()
        self.__fenster.geometry("300x540")

        # -------------------- Start-GUI

        # Platzierung des Frames für den Background
        self.__frameBackgroundSetup.place(x=0, y=0, width=300, height=540)

        # Platzierung des Frames für die Überschrift
        self.__frameUeberschriftSetup.place(x=10, y=10, width=280, height=80)

        # Platzierung des Labels für die Überschrift
        self.__labelUeberschriftSetup.place(x=10, y=10, width=260, height=60)

        # ---------- Lokales Spiel

        # Platzierung des Frames für das lokale Spiel
        self.__frameLocal.place(x=10, y=100, width=280, height=220)

        # Platzierung des Frames für die Überschrift des lokalen Spiels
        self.__frameUeberschriftLocal.place(x=10, y=10, width=260, height=50)

        # Platzierung des Labels für die Überschrift
        self.__labelUeberschriftLocal.place(x=0, y=0, width=260, height=50)

        # Platzierung der Dropdowns
        for i in range(4):
            self.__listDropdown[i].place(
                x=(10, 145, 145, 10)[i], y=(70, 120)[i // 2], width=125, height=40
            )

        # Platzierung des Buttons um das lokale Spiel zu starten
        self.__buttonStartLocal.place(x=10, y=170, width=260, height=40)

        # ---------- Globales Spiel

        # Platzierung des Frames für das globale Spiel
        self.__frameGlobalesSpiel.place(x=10, y=330, width=280, height=200)

        # Platzierung des Frames für die Überschrift des lokalen Spiels
        self.__frameUeberschriftGlobal.place(x=10, y=10, width=260, height=50)

        # Platzierung des Labels für die Überschrift
        self.__labelUeberschriftGlobal.place(x=0, y=0, width=260, height=50)

        # Platzierung des Labels für das Entry für die IP
        self.__labelIPentry.place(x=10, y=70, width=30, height=30)

        # Platzierung des Entrys für die IP für das globale Spiel
        self.__entryIP.place(x=50, y=70, width=220, height=30)

        # Platzierung des Labels für das Entry für den Port
        self.__labelPortentry.place(x=10, y=110, width=30, height=30)

        # Platzierung des Entrys für den Port für das globale Spiel
        self.__entryPort.place(x=50, y=110, width=220, height=30)

        # Platzierung des Buttons um das lokale Spiel zu starten
        self.__buttonGlobalesSpiel.place(x=10, y=150, width=260, height=40)

    def view_waiting(self):
        """Aufrufen der Lobby"""
        self.__frameBackgroundSetup.place_forget()
        self.__fenster.geometry("300x400")

        # Platzierung des Frames für den Background
        self.__backgroundLobbyGUI.place(x=0, y=0, width=300, height=400)

        # Platzierung der Frames für die Spieler
        for i in range(4):
            self.__listFramesSpielerLobby[i].place(
                x=45, y=(45 + (80 * i)), width=200, height=70
            )

        # Platzierung der Labels für die Ports
        for i in range(4):
            self.__listLabelsPorts[i].place(x=10, y=10, width=120, height=50)

        # Platzierung der Frames für die Images für die Verbindung
        for i in range(4):
            self.__listFramesConnections[i].place(x=140, y=10, width=50, height=50)

    def view_board(self):
        """Aufrufen des Spielbretts"""
        self.__backgroundLobbyGUI.place_forget()
        self.__fenster.geometry("670x670")

        # Setze Gewinner zurueck
        for i, track in enumerate(self.__listTracks):
            track.config(
                background=(
                    ("red", "blue", "green", "yellow")[i // 10]
                    if i % 10 == 0
                    else "white"
                )
            )

        # Platzierung des Frames für den Background
        self.__frameBackgroundGame.place(x=0, y=0, width=700, height=700)

        # Platzierung der Frames für die Homes
        for i in range(4):
            self.__listBackHomes[i].place(
                x=[10, 530, 530, 10][i], y=[10, 530][i // 2], width=130, height=130
            )

        # Platzierung Buttons für die Homes
        for i in range(4):
            for a in range(4):
                self.__listHomes[i][a].place(
                    x=[10, 70][a % 2], y=[10, 70][a // 2], width=50, height=50
                )

        # Platzierung der Labels für die Pfeile
        for i in range(4):
            self.__listLabelsArrows[i].place(
                x=[10, 430, 610, 190][i], y=[190, 10, 430, 610][i], width=50, height=50
            )

        # ---------- Platzierung der Frames für die Verbingung der Buttons
        # Platzierung des Frames für die Verbindung von Button 0 bis 4
        self.__listBackConnect[0].place(x=30, y=270, width=250, height=10)

        # Platzierung des Frames für die Verbindung von Button 4 bis 8
        self.__listBackConnect[1].place(x=270, y=30, width=10, height=250)

        # Platzierung des Frames für die Verbindung von Button 8 bis 10
        self.__listBackConnect[2].place(x=270, y=30, width=120, height=10)

        # Platzierung des Frames für die Verbindung von Button 10 bis 14
        self.__listBackConnect[3].place(x=390, y=30, width=10, height=250)

        # Platzierung des Frames für die Verbindung von Button 14 bis 18
        self.__listBackConnect[4].place(x=390, y=270, width=250, height=10)

        # Platzierung des Frames für die Verbindung von Button 18 bis 20
        self.__listBackConnect[5].place(x=630, y=270, width=10, height=120)

        # Platzierung des Frames für die Verbindung von Button 20 bis 24
        self.__listBackConnect[6].place(x=390, y=390, width=250, height=10)

        # Platzierung des Frames für die Verbindung von Button 24 bis 28
        self.__listBackConnect[7].place(x=390, y=390, width=10, height=250)

        # Platzierung des Frames für die Verbindung von Button 28 bis 30
        self.__listBackConnect[8].place(x=270, y=630, width=120, height=10)

        # Platzierung des Frames für die Verbindung von Button 30 bis 34
        self.__listBackConnect[9].place(x=270, y=390, width=10, height=250)

        # Platzierung des Frames für die Verbindung von Button 34 bis 38
        self.__listBackConnect[10].place(x=30, y=390, width=250, height=10)

        # Platzierung des Frames für die Verbindung von Button 18 bis 20
        self.__listBackConnect[11].place(x=30, y=270, width=10, height=120)

        # ---------- Platzierung der Buttons für die Tracks
        for i in range(40):
            if i < 5:
                self.__listTracks[i].place(x=10 + 60 * i, y=250, width=50, height=50)
            elif i < 9:
                self.__listTracks[i].place(
                    x=250, y=190 - 60 * (i - 5), width=50, height=50
                )
            elif i == 9:
                self.__listTracks[i].place(x=310, y=10, width=50, height=50)
            elif i < 15:
                self.__listTracks[i].place(
                    x=370, y=10 + 60 * (i - 10), width=50, height=50
                )
            elif i < 19:
                self.__listTracks[i].place(
                    x=430 + 60 * (i - 15), y=250, width=50, height=50
                )
            elif i < 21:
                self.__listTracks[i].place(
                    x=610, y=310 + 60 * (i - 19), width=50, height=50
                )
            elif i < 25:
                self.__listTracks[i].place(
                    x=550 - 60 * (i - 21), y=370, width=50, height=50
                )
            elif i < 29:
                self.__listTracks[i].place(
                    x=370, y=430 + 60 * (i - 25), width=50, height=50
                )
            elif i < 31:
                self.__listTracks[i].place(
                    x=310 - 60 * (i - 29), y=610, width=50, height=50
                )
            elif i < 35:
                self.__listTracks[i].place(
                    x=250, y=550 - 60 * (i - 31), width=50, height=50
                )
            elif i < 39:
                self.__listTracks[i].place(
                    x=190 - 60 * (i - 35), y=370, width=50, height=50
                )
            elif i == 39:
                self.__listTracks[i].place(x=10, y=310, width=50, height=50)

        # Platzierung der Frames für die Destinations
        for i in range(4):
            self.__listBackDest[i].place(
                x=[65, 305, 370, 305][i],
                y=[305, 65, 305, 370][i],
                width=[235, 60][i % 2],
                height=[60, 235][i % 2],
            )

        # Platzierung der Buttons für die Destinations
        for i in range(4):
            for a in range(4):
                self.__listDests[i][a].place(
                    x=[(5 + 60 * a), 5, (180 - 60 * a), 5][i],
                    y=[5, (5 + 60 * a), 5, (180 - 60 * a)][i],
                    width=50,
                    height=50,
                )

    def enable_client_connect(self):
        self.__buttonGlobalesSpiel.config(state="normal", text="Globales Spiel starten")

    def disable_client_connect(self):
        self.__buttonGlobalesSpiel.config(
            state="disable", text="Ungültige Spielkonfiguration"
        )

    def enable_start_game(self):
        """Button im Setup für das lokale Spiel auf "normal" stellen"""
        self.__buttonStartLocal.config(state="normal", text="Spiel starten")

    def disable_start_game(self):
        """Button im Setup auf "disable" stellen"""
        self.__buttonStartLocal.config(
            state="disable", text="Ungültige Spielkonfiguration"
        )

    def show_connection_info(self, player: int, port: int):
        """Freien Port für die Verbindung anzeigen"""
        self.__listLabelsPorts[player].config(text="Port zur Verbindung:\n" + str(port))
        self.__listFramesConnections[player].config(image=self.__not_connected)

    def set_player_ready(self, player: int):
        """Grafische Änderunge eines Bildes"""
        self.__listLabelsPorts[player].config(text="Spieler " + str(player))
        self.__listFramesConnections[player].config(image=self.__connected)

    def enable_client_connect(self):
        """Button im Setup für die Verbindung wird auf "normal" gesetzt"""
        self.__buttonGlobalesSpiel.config(state="normal", text="Spiel beitreten")

    def disable_client_connect(self):
        """Button im Setup für die Verbindung wird auf "disable" gesetzt"""
        self.__buttonGlobalesSpiel.config(state="disable", text="IP und Port eingeben")

    def report_error(self, message: str):
        """Fenster für eine mögliche Fehlermeldung"""
        self.__frameBackgroundSetup.place_forget()
        self.__backgroundLobbyGUI.place_forget()
        self.__frameBackgroundGame.place_forget()
        self.__fenster.geometry("500x500")

        # Platzierung des Hintergrunds
        self.__frameBackgroundError.place(x=0, y=0, width=500, height=500)

        # Platzierung des Labels für die Fehlermeldung
        self.__labelError.place(x=0, y=0, width=500, height=500)

        # Einfügen der Fehlermeldung
        self.__labelError.config(text=message)

    def enable_die(self):
        """Button für den Würfel im Spiel auf "normal" stellen"""
        self.__buttonWuerfelGame.config(state="normal")

    def disable_die(self):
        """Button für den Würfel im Spiel auf "disable" stellen"""
        self.__buttonWuerfelGame.config(state="disable")

    def update_die(self, pips):
        """Änderung des Textes des Buttons um eine neue Zahl anzuzeigen"""
        self.__buttonWuerfelGame.config(text=self.__dice[pips - 1])

    def set_active_player(self, player):
        """Verschieben des Würfels zu dem neuen aktiven Spieler"""
        self.__buttonWuerfelGame.place(
            x=(150, 470, 470, 150)[player],
            y=(150, 470)[player // 2],
            width=50,
            height=50,
        )

    def unset_active_player(self, player):
        """Hat keine Funktion mehr"""
        pass

    def vacate_home(self, owner, field):
        """Änderung eines Bildes, so das eine Figur von einem Homefeld weggenommen wird"""
        self.__listHomes[owner][field].config(image="")

    def fill_home(self, owner, field, piece):
        """Änderung eines Bildes, so das eine Figur auf ein Homefeld hingesetzt wird"""
        self.__listHomes[owner][field].config(image=self.__listPieces[piece])

    def vacate_start(self, owner):
        """Änderung eines Bildes, so das eine Figur von einem Startfeld weggenommen wird"""
        self.__listStarts[owner].config(image="")

    def fill_start(self, owner, piece):
        """Änderung eines Bildes, so das eine Figur auf ein Startfeld hingesetzt wird"""
        self.__listStarts[owner].config(image=self.__listPieces[piece])

    def vacate_track(self, field):
        """Änderung eines Bildes, so das eine Figur von einem Spielfeld(dem Track) weggenommen wird"""
        self.__listTracks[field].config(image="")

    def fill_track(self, field, piece):
        """Änderung eines Bildes, so das eine Figur auf ein Spielfeld(dem Track) hingesetzt wird"""
        self.__listTracks[field].config(image=self.__listPieces[piece])

    def vacate_dest(self, owner, field):
        """Änderung eines Bildes, so das eine Figur von einem Destinationfeld weggenommen wird"""
        self.__listDests[owner][field].config(image="")

    def fill_dest(self, owner, field, piece):
        """Änderung eines Bildes, so das eine Figur auf ein Destinationfeld hingesetzt wird"""
        self.__listDests[owner][field].config(image=self.__listPieces[piece])

    def announce_winner(self, player):
        """Änderung aller Spielfelder(Trackfelder) zu der Farbe des Gewinners"""
        for i in self.__listTracks:
            i.config(background=["red", "blue", "green", "yellow"][player])

    def run(self):
        """Aufrufen der GUI"""
        self.__fenster.mainloop()

    def sleep(self, milliseconds: int, callback):
        """Warten der GUI um eine übergebene Zeit"""
        self.__after_id = self.__fenster.after(milliseconds, callback)

    def cancel_sleep(self):
        """Breche einen geplanten callback ab."""
        self.__fenster.after_cancel(self.__after_id)

    def process(self):
        """Updaiten des Fensters"""
        self.__fenster.update()

    def quit(self):
        self.__fenster.destroy()
