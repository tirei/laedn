from abc import ABC, abstractmethod
from typing import Callable


class Actor(ABC):
    is_active = True

    def __init__(
        self,
        log,
        player_num: int,
        try_move_home_cb: Callable[[int, int], None],
        try_move_track_cb: Callable[[int], None],
        try_move_dest_cb: Callable[[int, int], None],
        activate_die_cb: Callable[[], None],
        process_view_cb: Callable[[], None],
        show_connection_info_cb: Callable[[int, int], None],
    ):
        """Initialisiere einen Akteur."""
        self._log = log.getChild("P#{}[{}]".format(player_num, type(self).__name__))
        self._player_num = player_num
        self._try_move_home_cb = try_move_home_cb
        self._try_move_track_cb = try_move_track_cb
        self._try_move_dest_cb = try_move_dest_cb
        self._activate_die_cb = activate_die_cb
        self._process_view_cb = process_view_cb
        self._show_connection_info_cb = show_connection_info_cb

    @abstractmethod
    def ready(self) -> bool:
        """Gibt zurück, ob dieser Akteur bereit ist."""
        raise NotImplementedError()

    def quit(self):
        """Beende den Akteur."""
        pass

    def end_turn(self, player: int):
        """Behandle ein Ereignis."""
        pass

    def begin_turn(self, player: int):
        """Behandle ein Ereignis."""
        pass

    def home_vacated(self, owner: int, field: int):
        """Behandle ein Ereignis."""
        pass

    def home_filled(self, owner: int, field: int, piece: int):
        """Behandle ein Ereignis."""
        pass

    def track_vacated(self, field: int):
        """Behandle ein Ereignis."""
        pass

    def track_filled(self, field: int, piece: int):
        """Behandle ein Ereignis."""
        pass

    def dest_vacated(self, owner: int, field: int):
        """Behandle ein Ereignis."""
        pass

    def dest_filled(self, owner: int, field: int, piece: int):
        """Behandle ein Ereignis."""
        pass

    def game_end(self, winner: int):
        """Behandle ein Ereignis."""
        pass

    def game_start(self):
        """Behandle ein Ereignis."""
        pass

    def click_roll_die(self):
        """Behandle ein Ereignis."""
        pass

    def click_move_home(self, owner: int, field: int):
        """Behandle ein Ereignis."""
        pass

    def click_move_track(self, field: int):
        """Behandle ein Ereignis."""
        pass

    def click_move_dest(self, owner: int, field: int):
        """Behandle ein Ereignis."""
        pass

    def allow_move(self):
        """Behandle ein Ereignis."""
        pass

    def update_die(self, pips: int):
        """Behandle ein Ereignis."""
        pass
