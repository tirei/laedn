import enum
import socket
from typing import Callable, Iterator, Tuple, Union

import actors.artificial
import actors.disabled
import actors.local
import actors.remote
import client
import model.game


class DispatchMode(enum.Enum):
    """Die Weisen, auf die Ereignisse verteilt werden können."""

    Setup = 0
    Local = 1
    Client = 2


class ActorDispatch(object):
    """Verteile Ereignisse.
    Verwalte hierzu die Aktoren, welche die Spieler kontrollieren.
    """

    __modes = [
        actors.disabled.DisabledActor,
        actors.local.LocalActor,
        actors.remote.RemoteActor,
        actors.artificial.RunnerActor,
    ]

    def __init__(
        self,
        log,
        home_vacated_cb: Callable[[int, int], None],
        home_filled_cb: Callable[[int, int, int], None],
        track_vacated_cb: Callable[[int], None],
        track_filled_cb: Callable[[int, int], None],
        dest_vacated_cb: Callable[[int, int], None],
        dest_filled_cb: Callable[[int, int, int], None],
        end_turn_cb: Callable[[int], None],
        begin_turn_cb: Callable[[int], None],
        game_end_cb: Callable[[int], None],
        game_start_cb: Callable[[], None],
        die_animation_cb: Callable[[], None],
        show_die_cb: Callable[[int], None],
        show_connection_info_cb: Callable[[int, int], None],
        actor_ready_cb: Callable[[int], None],
        report_error_cb: Callable[[str], None],
        process_view_cb: Callable[[], None],
    ):
        """Erstelle ein `ActorDispatch` Objekt."""
        self.__log = log.getChild("dispatch")
        self.__mode = DispatchMode.Setup
        self.__actors = []
        self.__active = None
        self.__local_game = None
        self.__client = None

        self.__home_vacated_cb = home_vacated_cb
        self.__home_filled_cb = home_filled_cb
        self.__track_vacated_cb = track_vacated_cb
        self.__track_filled_cb = track_filled_cb
        self.__dest_vacated_cb = dest_vacated_cb
        self.__dest_filled_cb = dest_filled_cb
        self.__end_turn_cb = end_turn_cb
        self.__begin_turn_cb = begin_turn_cb
        self.__game_end_cb = game_end_cb
        self.__game_start_cb = game_start_cb
        self.__die_animation_cb = die_animation_cb
        self.__show_die_cb = show_die_cb
        self.__show_connection_info_cb = show_connection_info_cb
        self.__actor_ready_cb = actor_ready_cb
        self.__process_view_cb = process_view_cb
        self.__report_error_cb = report_error_cb

    def init_server(self, log, players: Iterator[Tuple[int, int]]):
        """Initialisiere ein Server/Lokales Spiel."""
        self.__mode = DispatchMode.Local

        self.__local_game = self.__local_game or model.game.Game(
            log,
            self.__end_turn,
            self.__begin_turn,
            self.__home_vacated,
            self.__home_filled,
            self.__track_vacated,
            self.__track_filled,
            self.__dest_vacated,
            self.__dest_filled,
            self.__game_end,
        )
        players = list(self.__create_actors(players))
        self.__wait_for_actors()
        if self.__mode == DispatchMode.Setup:
            return
        self.__local_game.set_players(iter(players))

    def init_client(self, log, host: str, port: int) -> bool:
        """Initialisiere ein Spiel in Client-Rolle."""
        self.__mode = DispatchMode.Client
        connection = socket.socket()
        try:
            connection.connect((host, port))
        except OSError as ex:
            self.__report_error("Failed to connect: {}".format(str(ex)))
            return False
        self.__client = client.Client(
            log,
            connection,
            self.__end_turn,
            self.__begin_turn,
            self.__show_die,
            self.__home_vacated,
            self.__home_filled,
            self.__track_vacated,
            self.__track_filled,
            self.__dest_vacated,
            self.__dest_filled,
            self.__create_client_actors,
            self.__game_end,
            self.__process_view_cb,
            self.__report_error,
        )
        return True

    def run_client(self):
        """Starte den Client."""
        self.__client.run_client()

    # Controller -> Actor
    def click_roll_die(self):
        """Ein lokaler Nutzer fordert einen Würfelwurf an."""
        self.__active.click_roll_die()

    def click_move_home(self, owner: int, field: int):
        """Ein lokaler Nutzer fordert das Bewegen einer Figur an."""
        self.__active.click_move_home(owner, field)

    def click_move_track(self, field: int):
        """Ein lokaler Nutzer fordert das Bewegen einer Figur an."""
        self.__active.click_move_track(field)

    def click_move_dest(self, owner: int, field: int):
        """Ein lokaler Nutzer fordert das Bewegen einer Figur an."""
        self.__active.click_move_dest(owner, field)

    # Controller -> Game
    def allow_move(self):
        """Der Würfel wurde geworfen."""
        self.__game.allow_move()
        for actor in self.__actors:
            actor.allow_move()

    def roll_die(self) -> int:
        """Werfe den Würfel und gebe die Augenanzahl zurück."""
        # die_animation may only be called on server
        pips = self.__local_game.roll_die()
        for actor in self.__actors:
            actor.update_die(pips)
        return pips

    def reset(self) -> bool:
        """Setze dieses Objekt auf Setup zustand zurück."""
        if self.__mode == DispatchMode.Setup:
            return False
        if self.__local_game:
            self.__local_game.reset()
        for actor in self.__actors:
            actor.quit()
        mode = self.__mode
        self.__mode = DispatchMode.Setup
        if mode == DispatchMode.Client:
            self.__client.quit()
            self.__client = None
        self.__active = None
        self.__actors.clear()
        return True

    # Actor -> Game
    def __try_move_home(self, owner: int, field: int):
        """Ordere das Bewegen einer Figur an."""
        self.__game.try_move_home(owner, field)

    def __try_move_track(self, field: int):
        """Ordere das Bewegen einer Figur an."""
        self.__game.try_move_track(field)

    def __try_move_dest(self, owner: int, field: int):
        """Ordere das Bewegen einer Figur an."""
        self.__game.try_move_dest(owner, field)

    # Actor -> Controller
    def __show_die(self, pips: int):
        """Aktualisiere die Würfelanzeige."""
        self.__show_die_cb(pips)
        for actor in self.__actors:
            actor.update_die(pips)

    def __report_error(self, message: str):
        """Zeige eine Fehlermeldung an."""
        self.__mode = DispatchMode.Setup
        self.__report_error_cb(message)

    # Game -> Controller
    def __end_turn(self, player: int):
        """`player`'s Zug ist zu Ende."""
        self.__end_turn_cb(player)
        for actor in self.__actors:
            actor.end_turn(player)

    def __begin_turn(self, player: int):
        """`player`'s Zug beginnt."""
        if self.__mode == DispatchMode.Setup:
            return
        self.__active = self.__actors[player]
        self.__log.debug("active actor ({}): {}".format(player, self.__active))
        self.__begin_turn_cb(player)
        for actor in self.__actors:
            actor.begin_turn(player)

    def __home_vacated(self, owner: int, field: int):
        """Leere ein Feld."""
        self.__home_vacated_cb(owner, field)
        for actor in self.__actors:
            actor.home_vacated(owner, field)

    def __home_filled(self, owner: int, field: int, piece: int):
        """Fülle ein Feld."""
        self.__home_filled_cb(owner, field, piece)
        for actor in self.__actors:
            actor.home_filled(owner, field, piece)

    def __track_vacated(self, field: int):
        """Leere ein Feld."""
        self.__track_vacated_cb(field)
        for actor in self.__actors:
            actor.track_vacated(field)

    def __track_filled(self, field: int, piece: int):
        """Fülle ein Feld."""
        self.__track_filled_cb(field, piece)
        for actor in self.__actors:
            actor.track_filled(field, piece)

    def __dest_vacated(self, owner: int, field: int):
        """Leere ein Feld."""
        self.__dest_vacated_cb(owner, field)
        for actor in self.__actors:
            actor.dest_vacated(owner, field)

    def __dest_filled(self, owner: int, field: int, piece: int):
        """Fülle ein Feld."""
        self.__dest_filled_cb(owner, field, piece)
        for actor in self.__actors:
            actor.dest_filled(owner, field, piece)

    def __activate_die(self):
        """Fordere das Rollen des Würfels an."""
        if self.__mode == DispatchMode.Client:
            self.__game.activate_die()
        else:
            self.__die_animation_cb()

    def __game_end(self, winner: int):
        """`winner` hat gewonnen."""
        self.__game_end_cb(winner)
        for actor in self.__actors:
            actor.game_end(winner)

    # implementation
    @property
    def __game(self) -> Union[model.game.Game, client.Client]:
        """Client bzw. Game."""
        if self.__mode == DispatchMode.Client:
            return self.__client
        return self.__local_game

    def __client_actors(self, player_num: int) -> Iterator[Tuple[int, int]]:
        """Die Aktor-Modus-IDs für den Client."""
        for num in range(4):
            yield num, int(num == player_num)

    def __create_actors(
        self, players: Iterator[Tuple[int, int]]
    ) -> Iterator[Tuple[int, bool]]:
        """Erstelle die Aktoren nach `players`.
        Gebe die Spielerinformationen für `Game` zurück.
        """
        self.__log.debug("creating actors")
        for num, mode in players:
            mode = self.__modes[mode]
            self.__log.debug("creating {}".format(mode.__name__))
            actor = mode(
                self.__log,
                num,
                self.__try_move_home,
                self.__try_move_track,
                self.__try_move_dest,
                self.__activate_die,
                self.__process_view_cb,
                self.__show_connection_info_cb,
            )
            self.__actors.append(actor)
            yield (num, actor.is_active)

    def __create_client_actors(self):
        """Erstelle die Aktoren für den Client."""
        list(self.__create_actors(self.__client_actors(self.__client.player_num)))
        self.__wait_for_actors()

    def __wait_for_actors(self):
        """Warte bis alle Aktoren bereit sind, starte dann das Spiel."""
        self.__log.debug("waiting for actors")
        ready = [False] * 4
        while self.__mode != DispatchMode.Setup and not all(ready):
            for i, actor in enumerate(self.__actors):
                if not ready[i]:
                    ready[i] = actor.ready()
                    if ready[i]:
                        self.__actor_ready_cb(i)
                self.__process_view_cb()
        if self.__mode != DispatchMode.Setup:
            self.__log.debug("all actors ready")
            self.__game_start_cb()
            for actor in self.__actors:
                actor.game_start()
        else:
            self.__log.debug("cancel actor wait")
