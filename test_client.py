# A CLI byte client for the laedn server.
# This is a development tool.

import struct
import socket
import threading
import sys

s = socket.socket()
s.connect(("", int(input("port: "))))

def run_recv():
    while True:
        try:
            byte = s.recv(1)
        except:
            print("closed")
            break
        if len(byte) == 0:
            sys.exit(1)
        print("<", struct.unpack("!B", byte)[0])


def run_send():
    while True:
        try:
            byte = input()
        except:
            break
        try:
            byte = int(byte)
        except:
            continue
        if byte > 255:
            print("too large")
            continue
        s.sendall(struct.pack("!B", byte))

recv = threading.Thread(target=run_recv)
recv.start()
run_send()
s.shutdown(socket.SHUT_RDWR)
s.close()
recv.join()
